package main

import (
	"fmt"
	"github.com/hybridgroup/mjpeg"
	"github.com/labstack/echo"
	"gocv.io/x/gocv"
)

func main() {
	webcam, err := gocv.OpenVideoCapture("udp://192.168.178.78:49199")

	if err != nil {
		fmt.Println(err)
		fmt.Printf("Error opening capture device: %v\n", webcam)
	}

	defer webcam.Close()


	stream := mjpeg.NewStream()

	// start capturing
	go mjpegCapture(webcam, stream)

	e := echo.New()
	e.GET("/stream", echo.WrapHandler(stream))
	e.Logger.Fatal(e.Start(":8080"))
}


func mjpegCapture(webcam *gocv.VideoCapture, stream *mjpeg.Stream) {
	img := gocv.NewMat()
	defer img.Close()

	for {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("Device closed: %v\n", webcam)
			return
		}
		if img.Empty() {
			continue
		}

		buf, _ := gocv.IMEncode(".jpg", img)
		stream.UpdateJPEG(buf)
	}
}


