package camera

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/jshsj/golumix/internal/pkg/camera"
	"testing"
)


func TestGetAvailableShutterSpeeds(t *testing.T) {
	// Range to test
	//ShutterSpeed2 = 256
	//ShutterSpeed1_6 = 171
	//ShutterSpeed1_3 = 86
	//ShutterSpeed1 = 0
	//ShutterSpeed1_3s = -85
	//ShutterSpeed1_6s = -170
	//ShutterSpeed2s = -256
	availableSpeeds := camera.GetAvailableShutterSpeeds(256, -256)
	assert.Equal(t, availableSpeeds[0], 256)
	assert.Equal(t, camera.ShutterSpeedString(availableSpeeds[0]), "2")
	assert.Equal(t, len(availableSpeeds), 7)
	assert.Equal(t, camera.ToShutterSpeed(availableSpeeds[0]), "256/256")
}
