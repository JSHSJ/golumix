package camera


const (
	AF_TRACKING = "aftracking"
	AF_49AREA = "49area"
	AF_1AREA = "1area"
	AF_PINPOINT = "pinpoint"
	AF_FACEDETECTION = "facedection"
)

//var fDisplayString = map[int]string {
//	Focal1_7: "F 1.7",
//	Focal1_8: "F 1.8",
//	Focal2: "F 2",
//	Focal2_2: "F 2.2",
//	Focal2_5: "F 2.5",
//	Focal2_8: "F 2.8",
//	Focal3_2: "F 3.2",
//	Focal3_5: "F 3.5",
//	Focal4: "F 4",
//	Focal4_5: "F 4.5",
//	Focal5: "F 5",
//	Focal5_6: "F 6",
//	Focal6_3: "F 6.3",
//	Focal7_1: "F 7.1",
//	Focal8: "F 8",
//	Focal9: "F 9",
//	Focal10: "F 10",
//	Focal11: "F 11",
//	Focal13: "F 13",
//	Focal14: "F 14",
//	Focal16: "F 16",
//	Focal18: "F 18",
//	Focal20: "F 20",
//	Focal22 : "F 22",
//}
//
//var allFocals = []int {
//	Focal1_7,
//	Focal1_8,
//	Focal2,
//	Focal2_2,
//	Focal2_5,
//	Focal2_8,
//	Focal3_2,
//	Focal3_5,
//	Focal4,
//	Focal4_5,
//	Focal5,
//	Focal5_6,
//	Focal6_3,
//	Focal7_1,
//	Focal8,
//	Focal9,
//	Focal10,
//	Focal11,
//	Focal13,
//	Focal14,
//	Focal16,
//	Focal18,
//	Focal20,
//	Focal22,
//}
//
//func FocalString(focal int) string {
//	return fDisplayString[focal]
//}
//
//func ToFocal(focal int) string {
//	return strconv.Itoa(focal) + "/256"
//}
//
//func isFocalAvailable(focal int, max int, min int) bool {
//	return focal <= max && focal >= min
//}
//
//func GetAvailableFocalLengths(max int, min int) []int {
//	var availableFocalLengths []int
//
//	for i := range(allFocals) {
//		if isFocalAvailable(allFocals[i], max, min) {
//			availableFocalLengths = append(availableFocalLengths, allFocals[i])
//		}
//	}
//
//	return availableFocalLengths
//}
//
//
//func SetFocalSpeed(speed int, ip string) {
//	http.PostForm("http://" + ip + "/cam.cgi", url.Values{
//		"mode": {"setsetting"},
//		"type": {"focal"},
//		"value": {ToFocal(speed)},
//	})
//}
//
