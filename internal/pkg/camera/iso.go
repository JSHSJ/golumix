package camera

import (
"net/http"
"net/url"
"strconv"
)

const (
	ISO_I = -1
	ISO_AUTO = -2
	ISO_100 = 100
	ISO_125 = 125
	ISO_165 = 165
	ISO200 = 200
	ISO250 = 250
	ISO320 = 320
	ISO400 = 400
	ISO500 = 500
	ISO640 = 640
	ISO800 = 800
	ISO1000 = 1000
	ISO1250 = 1250
	ISO1600 = 1600
	ISO2000 = 2000
	ISO2500 = 2500
	ISO3200 = 3200
	ISO5000 = 5000
	ISO6400 = 6400
	ISO8000 = 8000
	ISO10000 = 10000
	ISO12800 = 12800
	ISO16000 = 16000
	ISO20000 = 20000
	ISO25600 = 25600
)

var allISOs = []int {
	ISO_I,
	ISO_AUTO,
	ISO_100,
	ISO_125,
	ISO_165,
	ISO200,
	ISO250,
	ISO320,
	ISO400,
	ISO500,
	ISO640,
	ISO800,
	ISO1000,
	ISO1250,
	ISO1600,
	ISO2000,
	ISO2500,
	ISO3200,
	ISO5000,
	ISO6400,
	ISO8000,
	ISO10000,
	ISO12800,
	ISO16000,
	ISO20000,
	ISO25600,
}

func ISOString(iso int) string {
	if iso == ISO_I { return "Intelligent" }
	if iso == ISO_AUTO { return "Auto"}
	return strconv.Itoa(iso)
}

func toISO(iso int) string {
	if iso == ISO_I { return "auto" }
	if iso == ISO_AUTO { return "i_iso"}

	return strconv.Itoa(iso)
}

func isISOAvailable(iso int, max int, min int) bool {
	return iso <= max && iso >= min
}

func GetAvailableISOs(max int, min int) []int {
	var availableISOs []int

	for i := range(allISOs) {
		if isISOAvailable(allISOs[i], max, min) {
			availableISOs = append(availableISOs, allISOs[i])
		}
	}

	availableISOs = append(availableISOs, ISO_I)
	availableISOs = append(availableISOs, ISO_AUTO)

	return availableISOs
}


func SetISO(iso int, ip string) {
	http.PostForm("http://" + ip + "/cam.cgi", url.Values{
		"mode": {"setsetting"},
		"type": {"iso"},
		"value": {toISO(iso)},
	})
}

