package camera

import (
	"net/http"
	"net/url"
	"strconv"
)

const (
	ShutterSpeed4000 = 3072
	ShutterSpeed3200 = 2987
	ShutterSpeed2500 = 2902
	ShutterSpeed2000 = 2816
	ShutterSpeed1600 = 2731
	ShutterSpeed1300 = 2646
	ShutterSpeed1000 = 2560
	ShutterSpeed800 =  2475
	ShutterSpeed640 = 2390
	ShutterSpeed500 = 2304
	ShutterSpeed400 = 2219
	ShutterSpeed320 = 2134
	ShutterSpeed250 = 2048
	ShutterSpeed200 = 1963
	ShutterSpeed160 = 1878
	ShutterSpeed125 = 1792
	ShutterSpeed100 = 1707
	ShutterSpeed80 = 1622
	ShutterSpeed60 = 1536
	ShutterSpeed50 = 1451
	ShutterSpeed40 = 1366
	ShutterSpeed30 = 1280
	ShutterSpeed25 = 1195
	ShutterSpeed20 = 1110
	ShutterSpeed15 = 1024
	ShutterSpeed13 = 939
	ShutterSpeed10 = 854
	ShutterSpeed8 = 768
	ShutterSpeed6 = 683
	ShutterSpeed5 = 598
	ShutterSpeed4 = 512
	ShutterSpeed3_2 = 426
	ShutterSpeed2_5 = 342
	ShutterSpeed2 = 256
	ShutterSpeed1_6 = 171
	ShutterSpeed1_3 = 86
	ShutterSpeed1 = 0
	ShutterSpeed1_3s = -85
	ShutterSpeed1_6s = -170
	ShutterSpeed2s = -256
	ShutterSpeed2_5s = -341
	ShutterSpeed3_2s = -426
	ShutterSpeed4s = -512
	ShutterSpeed5s = -597
	ShutterSpeed6s = -682
	ShutterSpeed8s = -768
	ShutterSpeed10s = -853
	ShutterSpeed13s = -938
	ShutterSpeed15s = -1024
	ShutterSpeed20s = -1109
	ShutterSpeed25s = -1194
	ShutterSpeed30s = -1280
	ShutterSpeed40s = -1375
	ShutterSpeed50s = -1450
	ShutterSpeed60s = -1536
)

var sPDisplayString = map[int]string {
	ShutterSpeed4000:  "4000",
	ShutterSpeed3200: "3200",
	ShutterSpeed2500: "2500",
	ShutterSpeed2000: "2000",
	ShutterSpeed1600: "1600",
	ShutterSpeed1300: "1300",
	ShutterSpeed1000: "1000",
	ShutterSpeed800: "800",
	ShutterSpeed640: "640",
	ShutterSpeed500: "500",
	ShutterSpeed400: "400",
	ShutterSpeed320: "320",
	ShutterSpeed250: "250",
	ShutterSpeed200: "200",
	ShutterSpeed160: "160",
	ShutterSpeed125: "125",
	ShutterSpeed100: "100",
	ShutterSpeed80: "80",
	ShutterSpeed60: "60",
	ShutterSpeed50: "50",
	ShutterSpeed40: "40",
	ShutterSpeed30: "30",
	ShutterSpeed25: "25",
	ShutterSpeed20: "20",
	ShutterSpeed15: "15",
	ShutterSpeed13: "13",
	ShutterSpeed10: "10",
	ShutterSpeed8: "8",
	ShutterSpeed6: "6",
	ShutterSpeed5: "5",
	ShutterSpeed4: "4",
	ShutterSpeed3_2: "3.2",
	ShutterSpeed2_5: "2.5",
	ShutterSpeed2: "2",
	ShutterSpeed1_6: "1.6",
	ShutterSpeed1_3: "1.3",
	ShutterSpeed1: "1",
	ShutterSpeed1_3s: "1.3s",
	ShutterSpeed1_6s: "1.6s",
	ShutterSpeed2s: "2s",
	ShutterSpeed2_5s: "2.5s",
	ShutterSpeed3_2s: "3.2s",
	ShutterSpeed4s: "4s",
	ShutterSpeed5s: "5s",
	ShutterSpeed6s: "6s",
	ShutterSpeed8s: "8s",
	ShutterSpeed10s: "10s",
	ShutterSpeed13s: "13s",
	ShutterSpeed15s: "15s",
	ShutterSpeed20s: "20s",
	ShutterSpeed25s: "25s",
	ShutterSpeed30s: "30s",
	ShutterSpeed40s: "40s",
	ShutterSpeed50s: "50s",
	ShutterSpeed60s: "60s",
}

var allSpeeds = []int {
	ShutterSpeed4000,
	ShutterSpeed3200,
	ShutterSpeed2500,
	ShutterSpeed2000,
	ShutterSpeed1600,
	ShutterSpeed1300,
	ShutterSpeed1000,
	ShutterSpeed800,
	ShutterSpeed640,
	ShutterSpeed500,
	ShutterSpeed400,
	ShutterSpeed320,
	ShutterSpeed250,
	ShutterSpeed200,
	ShutterSpeed160,
	ShutterSpeed125,
	ShutterSpeed100,
	ShutterSpeed80,
	ShutterSpeed60,
	ShutterSpeed50,
	ShutterSpeed40,
	ShutterSpeed30,
	ShutterSpeed25,
	ShutterSpeed20,
	ShutterSpeed15,
	ShutterSpeed13,
	ShutterSpeed10,
	ShutterSpeed8,
	ShutterSpeed6,
	ShutterSpeed5,
	ShutterSpeed4,
	ShutterSpeed3_2,
	ShutterSpeed2_5,
	ShutterSpeed2,
	ShutterSpeed1_6,
	ShutterSpeed1_3,
	ShutterSpeed1,
	ShutterSpeed1_3s,
	ShutterSpeed1_6s,
	ShutterSpeed2s,
	ShutterSpeed2_5s,
	ShutterSpeed3_2s,
	ShutterSpeed4s,
	ShutterSpeed5s,
	ShutterSpeed6s,
	ShutterSpeed8s,
	ShutterSpeed10s,
	ShutterSpeed13s,
	ShutterSpeed15s,
	ShutterSpeed20s,
	ShutterSpeed25s,
	ShutterSpeed30s,
	ShutterSpeed40s,
	ShutterSpeed50s,
	ShutterSpeed60s,
}

func ShutterSpeedString(speed int) string {
	return sPDisplayString[speed]
}

func ToShutterSpeed(speed int) string {
	return strconv.Itoa(speed) + "/256"
}

func isSpeedAvailable(speed int, max int, min int) bool {
	return speed <= max && speed >= min
}

func GetAvailableShutterSpeeds(max int, min int) []int {
	var availableShutterSpeeds []int

	for i := range(allSpeeds) {
		if isSpeedAvailable(allSpeeds[i], max, min) {
			availableShutterSpeeds = append(availableShutterSpeeds, allSpeeds[i])
		}
	}

	return availableShutterSpeeds
}


func SetShutterSpeed(speed int, ip string) {
	http.PostForm("http://" + ip + "/cam.cgi", url.Values{
		"mode": {"setsetting"},
		"type": {"shtrspeed"},
		"value": {ToShutterSpeed(speed)},
	})
}

