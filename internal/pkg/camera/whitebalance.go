package camera

const (
	WB_AUTO = "auto"
	WB_DAYLIGHT = "daylight"
	WB_CLOUDY = "cloudy"
	WB_SHADY = "shady"
	WB_HALOGEN = "halogen"
	WB_FLASH = "flash"
	WB_WHITE_SET_1 = "white_set1"
	WB_WHITE_SET_2 = "white_set2"
	WB_WHITE_SET_3 = "white_set3"
	WB_WHITE_SET_4 = "white_set4"
)

const (
	COLORTEMP = 0 // 2500 - 10000 (in 100)
)