package camera

import (
	"net/http"
	"time"
)

func KeepAlive() *time.Ticker {
	ip := "192.168.178.78"

	ticker := time.NewTicker(10000 * time.Millisecond)
	go func() {
		for _ = range ticker.C {
			http.Get("http://" + ip + "/cam.cgi?mode=getstate")
		}
	}()


	return ticker
}

func StopTicker(ticker *time.Ticker) {
	ticker.Stop()
}

func StartStream() {

	ip := "192.168.178.78"
	port := "49999"


	http.Get("http://" + ip + "/cam.cgi?mode=camcmd&value=recmode")
	http.Get("http://" + ip + "/cam.cgi?mode=startstream&value=" + port)

}
