package camera

const (
	FLASH_IAUTO = "iauto"
	FLASH_FORCED_ON = "forcedflashon"
	FLASH_FORCED_ON_REDEYE = "forced_on_redeye"
	FLASH_SLOWSYNC = "slowsync"
	FLASH_SLOWSYNC_REDEYE = "slowsyncredeye"
	FLASH_FORCED_OFF = "forcedflashoff"
)